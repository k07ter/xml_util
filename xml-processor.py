#!/usr/bin/python3

"""
__author__ = Konstantin Gorbunov kostin-g@yanex.ru

"""

import sys, os
from xml.etree import ElementTree as ET
from xml.dom import minidom

import random, string, zipfile
import argparse
import shutil, glob
import multiprocessing as mp
import csv

ARCHIVE_FOLDER = 'folder_in'
ARCHIVE_COUNTER = 50
FILES_IN_ARCHIVE = 100
UNIQ_NAMES = []

def get_new_string():
	letters = string.ascii_lowercase
	size = random.randrange(5, 20)
	line = ''.join([random.choice(letters) for code in range(size)])
	return line

def set_pair(prnt, sub_name, vlu1, value=None):
	var = ET.SubElement(prnt, sub_name)
	var.set('name', vlu1)
	if value:
		var.set('value', value)
	return var

def packToArhiv(arh_nom):
	zf_name = '%s/packet_%d.zip' % (ARCHIVE_FOLDER, arh_nom)
	zf = zipfile.ZipFile(zf_name, 'a')
	xmlToArh(zf)

def xmlToArh(zf):
	""" 
		The function that will packages xml-files into in archive.
		files_cnt: the number of files to be packed
	"""

	xml_list = random.sample(range(1, FILES_IN_ARCHIVE + 1), FILES_IN_ARCHIVE)

	for ech in xml_list:
		root = ET.Element('root')
		line_vlu = get_new_string()
		while (line_vlu in UNIQ_NAMES) or (line_vlu in os.listdir()):
			#print('data_' + line_vlu)
			line_vlu = get_new_string()

		var  = set_pair(root, 'var', 'id', value=line_vlu)
		var2 = set_pair(root, 'var', 'level', value=str(ech))

		objs = ET.SubElement(root, 'objects')
		objs_range_size = random.randrange(1, 11)

		for ech_obj in range(1, objs_range_size + 1):
			content_line = get_new_string()
			obj = set_pair(objs, 'object', content_line)

		file_suff = var.get('value')
		content = ET.ElementTree(root)
		fname = 'data_%s.xml' % file_suff
		content.write(fname)
		zf.write(fname)

	zf.close()

def xml_processing(filename):
	""" parsing of xml-file filename"""

	name_out = filename.split('.')[0] + '_%s.csv'
	v_ptr, o_ptr = [open(name_out % p, 'w') for p in ['vars', 'objs']]
	v_file = csv.writer(v_ptr)
	o_file = csv.writer(o_ptr)
	info = ET.parse(filename)
	root = info.getroot()

	try:
		_id = root.find("var[@name='id']").attrib['value']
		_level = root.find("var[@name='level']").attrib['value']
		_objects = [o.attrib['name'] for o in root.findall('objects/object')]
	except Exception as exc:
		print('Tag or attrib is not found')
		return

	v_file.writerow(['%s %s' % (_id, _level)])
	for obj_name in _objects:
	 	o_file.writerow(['%s %s' % (_id, obj_name)])

	v_ptr.close()
	o_ptr.close()

def unpack2folder(arh_ind):
	filename = '%s/packet_%d.zip' % (ARCHIVE_FOLDER, arh_ind)
	zf = zipfile.ZipFile(filename, 'r')
	pth = filename.split('.')[0]
	if os.path.isdir(pth):
		shutil.rmtree(pth)
	zf.extractall(pth)
	return pth

def parseToCSV(arh_ind):
	pth = unpack2folder(arh_ind)
	for f in os.listdir(pth):
		xml_processing(pth + '/' +f)

def launch_process(cmd):
	pool_que = []
	with mp.Pool() as p:
		for x in range(1, ARCHIVE_COUNTER):
			proc = p.Process(target=cmd, args=(x,))
			proc.start()
			proc.join()

	for f in glob.glob('data_*.xml'):
		os.remove(f)

def main(args):
	arch_cnt = 10
	if '--build' in sys.argv:
		cmd = packToArhiv
		if os.path.exists(ARCHIVE_FOLDER):
			if os.listdir(ARCHIVE_FOLDER):
				print('Directory is not empty')
				shutil.rmtree(ARCHIVE_FOLDER)
		else:
			os.mkdir(ARCHIVE_FOLDER)
	elif '--parse' in sys.argv:
		cmd = parseToCSV
	else:
		parser.print_help()

	launch_process(cmd)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='xml-processor: app for generation and parsing of data.')
	parser.add_argument('--build', action='store_true', help='To generate folder with packed xml-files')
	parser.add_argument('--parse', action='store_true', help='To parse zip-files from folder. Default it is "folder_in"')
	args = parser.parse_args()

	main(args)
